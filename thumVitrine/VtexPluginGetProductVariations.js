function limparExecesso(){
	var qtdDivs;
	$('.prateleira.vitrine.n4colunas li').each(function() {
		qtdDivs = $(this).find('.product-variation').length;
		if (qtdDivs > 1) {
			$(this).find('.product-variation').not($(this).find('.product-variation').last()).each(function(i){
				$(this).remove();
			})
		}
	});
}

$(document).ready(function() {

	$('body').delegate('a.vejamais-vtexresearch', 'click', function() {
		setTimeout(function(){
			VtexPluginGetProductVariations(".vitrine", "ul li");
			limparExecesso();
		},1000);
	});

});

var VtexPluginGetProductVariations;
VtexPluginGetProductVariations = function(vitrine_html_element, html_child_element) {
	$(".prateleira.vitrine ul li").each(function() {
		if (!$(this).find('div').hasClass('product-variation')) {
			$(this).append("<div class='product-variation'></div>");
		}
	});	
	$(".prateleira.vitrine ul li.helperComplement").length > 0 && $(".helperComplement").remove(), null == vitrine_html_element && (vitrine_html_element = ".vitrine"), null == html_child_element && (html_child_element = ".prateleira li"), $(vitrine_html_element).find(html_child_element).each(function() {
		if ($(this).find(".product-variation").length > 0) {
			var $product_element_object, product_id;
			$product_element_object = $(this), $product_element_object.find("input.pdt-id").length > 0 && (product_id = $product_element_object.find("input.pdt-id").val(), $.ajax("/api/catalog_system/pub/products/variations/" + product_id, {
				type: "GET",
				dataType: "json",
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("AJAX Error: " + textStatus)
				},
				success: function(data, textStatus, jqXHR) {
					
					var $cotainer_variation, before, i, main_thumb_src, sku, sku_thumb, skus;
					
					for (skus = data.skus, $cotainer_variation = $('<ul class="product-variation"></ul>').appendTo($product_element_object.find(".variations")), i = 0; i < skus.length;) before = i - 1, (0 === i || "undefined" != typeof skus[before] && skus[i].dimensions['Cor do SKU'] !== skus[before].dimensions['Cor do SKU'] && $cotainer_variation.find('li[data-sku-color="' + skus[i].dimensions['Cor do SKU'] + '"]').length < 1) && (sku_thumb = skus[i].image, sku = skus[i].sku, $cotainer_variation.append('<li id="variacao-item-' + sku + '" data-sku-color="' + skus[i].dimensions['Cor do SKU'] + '" data-sku-size="' + skus[i].dimensions.Tamanhos + '" data-sku="' + sku + '"> <div class="wrap-image"><img class="product-variation-image" data-sku="' + sku + '" src="' + sku_thumb.replace("392-392", "30-30") + '"/></div> <div class="wrap-info"><div class="title">' + skus[i].skuname + '</div> <div class="price"> <div class="price-from">' + skus[i].listPrice + '</div> <div class="price-current">' + skus[i].bestPrice + "</div> </div> </li>"), main_thumb_src = $product_element_object.find(".thumb a img").attr("src"), $("li#variacao-item-" + sku).mouseover(function() {
						
						$product_element_object.find("a img").attr("src", $(this).find("img").attr("src").replace("338-338", "169-326"))
					}), $("li#variacao-item-" + sku).mouseleave(function() {
						$product_element_object.find("a img").attr("src", main_thumb_src)
					})), i++
				}
			}))
		}
	})
}

$(document).one("ajaxStop", function() {
	setTimeout(function(){
		VtexPluginGetProductVariations(".vitrine", "ul li");	
	},1000)
	
})

