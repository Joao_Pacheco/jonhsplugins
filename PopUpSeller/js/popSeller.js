$(document).ready(function() {

	if ($('body').hasClass('produto')) {
		
		$("<div class='popSellerMascara' style='display:none;'></div>").appendTo('body');
		$("<div class='popSeller' style='display:none;'></div>").appendTo('body');

		setTimeout(function(){

			$('.seller-name a').click(function(e) {
				e.preventDefault();

				linkSeller = $('.seller-name a').attr('href');

			    var left = ($(window).width() /2) - ( $('.popSeller').width() / 2 );
		        var top = 100;
		     
		        $('.popSeller').css({'top':top,'left':left});
		        $('.popSeller').html(" ");
		        $('.popSeller').show();
		        $('.popSellerMascara').show();
			    $(".popSeller").load(linkSeller +"& .page-container .page-center");

				setTimeout(function(){
					$("<div class='popSellerFechar'>X Fechar</div>").appendTo('.popSeller');
					$('.popSellerFechar').click(function() {
						$('.popSeller').fadeOut();
						$('.popSellerMascara').fadeOut();

					});
					$('.popSellerMascara').click(function() {
						$('.popSeller').fadeOut();
						$('.popSellerMascara').fadeOut();

					});
					$('.popSeller .seller-description').prepend("<h2 class='tituloVendedor'>"+ $('.seller-name a').text() +"</h2>");

				},500);
			});
		},1000);
	}
});

